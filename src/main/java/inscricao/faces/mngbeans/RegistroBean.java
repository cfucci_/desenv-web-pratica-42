/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Christian
 */
@Named(value = "registroBean")
@ApplicationScoped
public class RegistroBean extends PageBean{
    
    private ArrayList<Candidato> candidatosList;

    /**
     * Creates a new instance of RegistroBean
     */
    public RegistroBean() {
        candidatosList = new ArrayList<Candidato>();
    }
    
    public void addCandidato(Candidato c){
        candidatosList.add(c);
    }
    
}
